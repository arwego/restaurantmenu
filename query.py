from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant, MenuItem

engine = create_engine('mysql+mysqldb://root:inmyLIFE1395@localhost/restaurantmenu')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

def list_restaurant():
    results = session.query(Restaurant).all()
    return results

def add_new_restaurant(restaurant_name):
    newRestaurant = Restaurant(name = restaurant_name)
    session.add(newRestaurant)
    session.commit()

def find_restaurant(restaurant_id):
    current = session.query(Restaurant).filter_by(id = restaurant_id).one()
    return current

def edit_restaurant(restaurant_id, restaurant_name):
    current = session.query(Restaurant).filter_by(id = restaurant_id).one()
    current.name = restaurant_name
    session.add(current)
    session.commit()

def delete_restaurant(restaurant_id):
    current = session.query(Restaurant).filter_by(id = restaurant_id).one()
    session.delete(current)
    session.commit()
