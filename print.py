from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database_setup import Restaurant, Base, MenuItem

engine = create_engine('mysql+mysqldb://root:inmyLIFE1395@localhost/restaurantmenu')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

items = session.query(Restaurant).all()
for item in items:
    print item.name

menus = session.query(MenuItem).all()
for menu in menus:
    print menu.name
