from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker

from database_setup import Restaurant, Base, MenuItem

engine = create_engine('mysql+mysqldb://root:inmyLIFE1395@localhost/restaurantmenu')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()

urbanBurger = session.query(Restaurant).filter_by(id = 1).one()
print urbanBurger.name
urbanBurger.name = "Urban Burger"
session.add(urbanBurger)
session.commit()

current = session.query(Restaurant).filter_by(id = 1).one()
print current.name
