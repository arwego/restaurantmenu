#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant, MenuItem

engine = create_engine('mysql+mysqldb://root:inmyLIFE1395@localhost/restaurantmenu')
Base.metadata.bind = engine
DBSession = sessionmaker(bind = engine)
session = DBSession()

myFirstRestaurant = Restaurant(name = "Pizza Palace")
session.add(myFirstRestaurant)
session.commit()
print session.query(Restaurant).all()

cheesePizza = MenuItem(name = "Cheese Pizza", course = 'Entree', description = "Made with natural ingredients and mozzarella", price = "$8.99", restaurant = myFirstRestaurant)
session.add(cheesePizza)
session.commit()
print session.query(MenuItem).all()

firstResult = session.query(Restaurant).first()
print firstResult.name
