from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import cgi
import query

class webserverHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path.endswith("/restaurants"):
                self.send_response(200)
                self.send_header('content-type', 'text/html')
                self.end_headers()

                listRestaurant = query.list_restaurant()
                output = "<html><body>"
                output += "<a href='/restaurants/new'>Make a new restaurant here</a><br /> <br />"
                for current in listRestaurant:
                    output += "<p>"
                    output += str(current.name)
                    output += "<br />"
                    output += "<a href = '/restaurants/%s/edit'>Edit</a>" % current.id
                    output += "<br />"
                    output += "<a href = '/restaurants/%s/delete'>Delete</a>" % current.id
                    output += "</p>"
                output += "</body></html>"

                self.wfile.write(output)
                # print output
                return

            if self.path.endswith("/edit"):
                restaurantIDPath = self.path.split("/")[2]
                current = query.find_restaurant(restaurantIDPath)
                if current != []:
                    self.send_response(200)
                    self.send_header('content-type', 'text/html')
                    self.end_headers()
                    output = "<html><body>"
                    output += "<h1>%s</h1>" % str(current.name)
                    output += '''<form method="POST" enctype="multipart/form-data" action="/restaurants/%s/edit">''' % restaurantIDPath
                    output += '''<input name="newRestaurantName" type="text" placeholder="%s">''' % str(current.name)
                    output += '''<input type="submit" value="Edit"></form>'''
                    output += "</body></html>"

                    self.wfile.write(output)
                return

            if self.path.endswith("/delete"):
                restaurantIDPath = self.path.split("/")[2]
                current = query.find_restaurant(restaurantIDPath)
                if current != []:
                    self.send_response(200)
                    self.send_header('content-type', 'text/html')
                    self.end_headers()
                    output = "<html><body>"
                    output += "<h1>Are you sure, you want to delete %s?</h1>" % str(current.name)
                    output += '''<form method="POST" enctype="multipart/form-data" action="/restaurants/%s/delete">''' % restaurantIDPath
                    output += '''<input type="submit" value="Delete"></form>'''
                    output += "</body></html>"

                    self.wfile.write(output)
                return

            if self.path.endswith("/restaurants/new"):
                self.send_response(200)
                self.send_header('content-type', 'text/html')
                self.end_headers()

                output = "<html><body>"
                output += "<h1>Make a new restaurant</h1>"
                output += '''<form method="POST" enctype="multipart/form-data" action="/restaurants/new">'''
                output += '''<input name="newRestaurantName" type="text" placeholder="New Restaurant Name">'''
                output += '''<input type="submit" value="Create"></form>'''
                output += "</body></html>"

                self.wfile.write(output)
                # print output
                return

        except IOError:
            self.send_error(404, "File not found {}".format(self.path))

    def do_POST(self):
        try:
            if self.path.endswith("/restaurants/new"):
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
                if ctype == 'multipart/form-data':
                    fields = cgi.parse_multipart(self.rfile, pdict)
                    messagecontent = fields.get('newRestaurantName')
                    query.add_new_restaurant(messagecontent[0])

                    self.send_response(301)
                    self.send_header('Content-type', 'text/html')
                    self.send_header('Location', '/restaurants')
                    self.end_headers()

            if self.path.endswith("/edit"):
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
                if ctype == 'multipart/form-data':
                    fields = cgi.parse_multipart(self.rfile, pdict)
                    messagecontent = fields.get('newRestaurantName')

                    restaurantIDPath = self.path.split("/")[2]
                    query.edit_restaurant(restaurantIDPath, messagecontent[0])

                    self.send_response(301)
                    self.send_header('Content-type', 'text/html')
                    self.send_header('Location', '/restaurants')
                    self.end_headers()

            if self.path.endswith("/delete"):
                restaurantIDPath = self.path.split("/")[2]
                query.delete_restaurant(restaurantIDPath)

                self.send_response(301)
                self.send_header('Content-type', 'text/html')
                self.send_header('Location', '/restaurants')
                self.end_headers()

        except:
            pass


def main():
    try:
        port = 8080
        server = HTTPServer(('', port), webserverHandler)
        print "Web server running on port {}".format(port)
        server.serve_forever()

    except KeyboardInterrupt:
        print " entered, stopping web server..."
        server.socket.close()

if __name__ == '__main__':
    main()
